# CodeHub Plugin

[codehub插件下载](http://szxy1.artifactory.cd-cloud-artifact.tools.huawei.com/artifactory/sz-maven-release/org/jenkins-ci/plugins/codehub-plugin/1.5.13.huawei.1/)

- [说明](#说明)
- [全局配置](#全局配置)
  - [Codehub-to-Jenkins认证](#codehub-to-jenkins认证)
  - [Jenkins-to-CodeHub认证](#codehub-to-jenkins认证)
- [Jenkins Job配置](#jenkins-job配置)
   - [Git配置](#git配置)
     - [Freestyle jobs](#freestyle-jobs)
     - [Pipeline jobs](#pipeline-jobs)
   - [Job触发器配置](#job触发器配置)
     - [Freestyle and Pipeline jobs](#freestyle-and-pipeline-jobs)
   - [构建状态配置](#构建状态配置)
     - [Freestyle jobs](#freestyle-jobs)
     - [Pipeline jobs](#pipeline-jobs)
- [定义变量](#定义变量)     
- [高级功能](#高级功能)
  - [分支过滤](#分支过滤)
  - [push tag触发构建](#push-tag触发构建)
  - [MergeRequest添加评论](#mergerequest添加评论)
  - [接收合并请求](#接收合并请求)

# 说明

此插件提供在CodeHub上提交代码或新建/更新合MergeRequest时触发Jenkins构建，并可以将构建状态发送回CodeHub的功能。


# 全局配置

## Codehub-to-Jenkins认证

默认情况下，该插件将要求为从CodeHub到Jenkins的连接设置身份验证，以防止未经授权的人员触发工作。 

###  配置全局认证
- 1.在Jenkins中创建一个至少具有Job权限的用户
- 2.以该用户身份登录（即使您是Jenkins管理员用户，这也是必需的），然后单击页面右上角的用户名。
- 3.单击“配置”，然后单击“显示API Token”，并记下/复制用户ID和API令牌
- 4.在CodeHub中，当您创建Webhooks来触发Jenkins Job时，请使用以下格式的URL，并且不要为“Secret Token”输入任何内容：`http：//USERID：APITOKEN@JENKINS_URL/project/YOUR_JOB`
- 5.添加Webhook之后，单击“测试”按钮，它应该成功

### 配置项目级认证

如果要为每个Jenkins job创建单独的身份验证凭据，请执行以下操作：

- 1.在Jenkins job配置中，在CodeHub配置部分中，单击“Advanced”
- 2.单击“Secret Token”字段下的“Generate”按钮
- 3.复制结果令牌，然后保存作业配置
- 4.在CodeHub中，为您的项目创建一个Webhook，输入触发URL（例如`http：//JENKINS_URL/project/YOUR_JOB`），然后将令牌粘贴到Secret Token字段中
- 5.添加Webhook之后，单击“测试”按钮，它应该成功


### 禁用身份验证

如果要禁用此身份验证（不建议）：

- 1.在Jenkins中，转到Manage Jenkins-> Configure System
- 2.向下滚动到标有“CodeHub”的部分
- 3.取消选中“为'/project'端点启用身份验证”-您现在可以从CodeHub触发Jenkins作业，而无需身份验证

## Jenkins-to-CodeHub认证

**请注意：** 此身份验证配置仅用于访问CodeHub API以将构建状态发送到CodeHub. 并不是用于代码下载.

可以将该插件配置为向CodeHub发送构建状态消息，该消息会显示在CodeHub合并请求页面中。要启用此功能：

- 1.授予该用户需有“Developer”及以上权限。
- 2.在CodeHub上生成access token(scope需勾选"api)
- 3.使用步骤2生成的access token，在Jenkins上添加"CodeHub API token"类型凭据
- 4.在Jenkins的“系统设置”页面上，添加CodeHub Server.

# Jenkins Job配置

使用CodeHub触发Jenkins Job时，一般需要修改Jenkins job的两个方面的配置。 

- 第一个是Git配置。 当CodeHub触发构建时，CodeHub插件将设置一些环境变量，您可以使用它们来控制从CodeHub下载什么代码。
- 第二个是用于将构建状态发送回CodeHub的配置，在提交代码和新建/更新MergeRequest时，触发jenkins job并发送构建状态给CodeHub。

## Git配置 

### Freestyle jobs
在*源码管理*配置区域:

1. 选择*Git*
2. 填写代码仓地址, 比如`https://codehub-dg-g.huawei.com/alphago/testwebhook.git`
    - 1.在*Advanced* 高级设置里, 可设置*Name*为``origin``
    - 2 设置*Refspec*为``+refs/heads/*:refs/remotes/origin/* +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*``
3. 在*Branch Specifier*选择构建分支:
    1. 分支模式MR: ``origin/${codehubSourceBranch}``
    2. Fork模式MR: ``merge-requests/${codehubMergeRequestIid}``
4. 在*Additional Behaviours*:
    1. 选择*Merge before build*
    2. 设置`Name of repository`仓库remote名为``origin``
    4. 设置`Branch to merge`合并到的分支为``${codehubTargetBranch}``

### Pipeline jobs

Pipeline类型job可在script加班下载指定分支/ref的代码，如:

```
checkout changelog: true, poll: true, scm: [
    $class: 'GitSCM',
    branches: [[name: "origin/${env.codehubSourceBranch}"]],
    doGenerateSubmoduleConfigurations: false,
    extensions: [[$class: 'PreBuildMerge', options: [fastForwardMode: 'FF', mergeRemote: 'origin', mergeStrategy: 'DEFAULT', mergeTarget: "${env.codehubTargetBranch}"]]],
    submoduleCfg: [],
    userRemoteConfigs: [[name: 'origin', url: 'https://codehub-dg-g.huawei.com/alphago/testwebhook.git']]
    ]
```
## Job触发器配置

### Freestyle and Pipeline jobs

1. 设置*Build Triggers*:
    * 勾选*Build when a change is pushed to CodeHub*
    * 生成默认的*CodeHub webhook URL*
    * 勾选*Push Events* 或者 *Created Merge Request Events* 或者 *Accepted Merge Request Events* 或者 *Closed Merge Request Events*
    * 可以选择性设置启用*Rebuild open Merge Requests* 来实现在推送到源分支之后重新构建打开的合并请求
2. 根据需要配置其他构建前，构建或构建后操作


## 构建状态配置

### Freestyle jobs

可以在`Post-build Actions`里添加'Publish build status to CodeHub'该action来发送构建状态给CodeHub
- 'Pending' 触发构建时发送.
- 'Running' 构建开始时发送.
- 'Success' or 'Failed'  构建结束时发送.


### Pipeline jobs

* 可以使用`updateGitlabCommitStatus`来更新构建状态
    ```
    node() {
        stage('Checkout') { checkout <your-scm-config> }

        updateGitlabCommitStatus name: 'build', state: 'pending'
        // Build steps
        
        updateGitlabCommitStatus name: 'build', state: 'success'
    }
    ```
# 定义变量

CodeHub Jenkins插件支持解析CodeHub webhook消息结构体，并将解析的数据设置为jenkins变量，你可以在jenkins job里引用这些变量。

- codehubMergeRequestTitle: MR标题
- codehubMergeRequestDescription: MR描述
- codehubMergeRequestId: MR id，MR数据库记录Id
- codehubMergeRequestIid: MR iid，MR internal id, 每个仓库的mr iid是从1开始递增(每次递增1)
- codehubMergeRequestState: MR状态，如opened、merged、closed
- codehubMergedByUser:  MR合入人
- codehubMergeRequestAssignee： MR待处理人
- codehubMergeRequestLastCommit: MR最后一个commit
- codehubMergeRequestTargetProjectId: MR目标仓库id
- codehubActionType: Action类型, 有如下几个Action:
     - PUSH：  
     - TAG_PUSH
     - MERGE
     - NOTE
     - PIPELINE
- codehubSourceBranch: MR源分支
- codehubSourceRepoHomepage
- codehubSourceRepoName： MR源仓库名
- codehubSourceNamespace： MR源仓库namespace
- codehubSourceRepoURL： MR源仓库路径
- codehubSourceRepoSshUrl: MR源仓库ssh下载地址
- codehubSourceRepoHttpUrl: MR源仓库http下载地址
- codehubTargetBranch: MR目标分支
- codehubTargetRepoName： MR目标仓库名
- codehubTargetNamespace： MR目标仓库namespace
- codehubTargetRepoSshUrl： MR目标ssh下载地址
- codehubTargetRepoHttpUrl： MR目标http下载地址
- codehubOmegaType:  Omega类型,非omegamr，该字段值为空:
     - root:   root mr
     - sub:    sub mr
- codehubManifestRepo: manifest仓库, 如alphago/omega/manifest
- codehubManifestFile: manifest文件， 如default.xml
- codehubManifestBranch: manifest分支， 如master
- codehubOmegaTopicSha:  omega mr的last commit id, 如`00c7e18c93d5d2fb3a1a2832511bc576f115393b`
- codehubOmegaSourceRef: omega mr的ref值，如`refs/omega/1b/1b97741d-e10a-412c-aebf-4a590af88f9f`



# 高级功能

## 分支过滤

可以根据分支名称过滤触发器，即仅允许选定分支使用构建。 在项目配置页面上，配置CodeHub触发器时，可以选择“按名称过滤分支”或“按正则表达式过滤分支”。 
按名称过滤将以逗号分隔的分支名称列表包括在内和/或排除在触发构建之外。 按正则表达式过滤采用Java正则表达式来包含和/或排除。

## push tag触发构建

push tag时触发jenkins构建:

1. CodeHub webhook勾选 'Tag push events'
2. 在源码管理区域:
    1. 设置`Advanced`，Refspec指定`+refs/tags/*:refs/remotes/origin/tags/*`， 增加/指定下载tag ref
    2. 设置`Branch Specifier`, 可以指定构建tag, 如`refs/tags/${TAGNAME}`

## MergeRequest添加评论

可以构建完成后向CodeHub MergeRequest添加评论。

在"Post-build actions"里可以添加"Add note with build status on CodeHub merge requests"来实现构建后向CodeHub MergeRequest添加评论。

## 接收合并请求

可以在构建成功后自动合并MergeRequest。

在"Post-build actions"里可以添加"Accept CodeHub merge request on success"来实现构建后构建成功后自动合并MergeRequest。

### Pipeline jobs

pipeline jobs有两个高级配置选项

1. **useMRDescription** -   将合并请求描述添加到Merge Commit提交中。
2. **removeSourceBranch** - 接收MR合并请求后，删除CodeHub源分支。

```
acceptGitLabMR(useMRDescription: true, removeSourceBranch: true)
```









